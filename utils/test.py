from rest_framework.test import APITestCase

from apps.v1.accounts.models import User


class SMSTestCase(APITestCase):
    """ Initial test setup and stuff for the tests """

    def setUp(self):
        self.email = "sms@email.com"
        self.password = "password"
        user = User.objects.create(email=self.email, is_staff=True)
        user.set_password(self.password)
        user.save()
        self.client.login(email=self.email, password=self.password)
