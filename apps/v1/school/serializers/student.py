from rest_framework import serializers

from apps.v1.school.models import Student, School


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'grade', 'section',)

    def create(self, validated_data):
        school_id = self.context.get('school_id')
        if school_id:
            validated_data['school'] = School.objects.get(id=school_id)
        return super().create(validated_data)

    def get_fields(self):
        fields = super().get_fields()
        request = self.context.get('request')
        if request and request.method == 'GET':
            fields['school'] = serializers.SerializerMethodField()
        return fields

    def get_school(self, instance):
        return {
            'name': instance.school.name,
            'id': instance.school.id
        }

