from rest_framework import serializers

from apps.v1.school.models import School


class SchoolSerializer(serializers.ModelSerializer):

    class Meta:
        model = School
        fields = ('name', 'description', 'established_date', 'id')

