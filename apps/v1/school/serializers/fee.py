from rest_framework import serializers

from apps.v1.school.models import Fee, Student


class FeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fee
        fields = ('due_amount', 'paid_amount', 'month', 'year',)

    def create(self, validated_data):
        student_id = self.context.get('student_id')
        if student_id:
            validated_data['student'] = Student.objects.get(id=student_id)
        return super().create(validated_data)

    def get_fields(self):
        fields = super().get_fields()
        request = self.context.get('request')
        if request and request.method == 'GET':
            fields['student'] = serializers.SerializerMethodField()
        return fields

    def get_student(self, instance):
        return {
            'name': instance.student.full_name,
            'id': instance.student.id
        }