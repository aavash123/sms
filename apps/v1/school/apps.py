from django.apps import AppConfig


class SchoolConfig(AppConfig):
    name = 'apps.v1.school'
