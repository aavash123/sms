from django.db import models
from django.utils.dates import MONTHS

from utils.constants import GRADE_CHOCIES, SECTION_CHOICES

from utils.year_choice_gen import year_choices, current_year

YEAR_CHOICES = year_choices()


class School(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=2500)
    established_date = models.DateField()

    def __str__(self):
        return "School > {}".format(self.name)


class Student(models.Model):
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    grade = models.CharField(max_length=5, choices=GRADE_CHOCIES)
    section = models.CharField(max_length=2, choices=SECTION_CHOICES)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)


class Fee(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, )
    due_amount = models.FloatField(null=True, )
    paid_amount = models.FloatField(null=True)
    paid_date = models.DateTimeField(auto_now=True)
    month = models.PositiveSmallIntegerField(choices=MONTHS.items())
    year = models.IntegerField(choices=YEAR_CHOICES, default=current_year)

    def __str__(self):
        return "Fee () for {} {}".format(self.student.full_name, self.year, self.month)
