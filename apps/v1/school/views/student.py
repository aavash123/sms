from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import get_object_or_404

from apps.v1.school.models import Student, School
from apps.v1.school.serializers.student import StudentSerializer


class StudentViewSet(viewsets.ModelViewSet):

    """ ViewSet for crud of student model"""

    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('first_name', )
    ordering_fields = ('first_name', )

    def filter_queryset(self, queryset):
        get_object_or_404(School, id=self.kwargs['school_id'])
        queryset = queryset.filter(school__id=self.kwargs['school_id'])
        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        return {
            'request': self.request,
            'school_id': self.kwargs.get('school_id')
        }
