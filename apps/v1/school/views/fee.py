from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import get_object_or_404

from apps.v1.school.models import Fee, Student
from apps.v1.school.serializers.fee import FeeSerializer


class FeeViewSet(viewsets.ModelViewSet):

    """ ViewSet for crud of fee model """

    queryset = Fee.objects.all()
    serializer_class = FeeSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('month', 'year', )
    ordering_fields = ('month', 'year', )

    def filter_queryset(self, queryset):
        get_object_or_404(Student, id=self.kwargs['student_id'])
        queryset = queryset.filter(student__id=self.kwargs['student_id'])
        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        return {
            'request': self.request,
            'student_id': self.kwargs.get('student_id')
        }
