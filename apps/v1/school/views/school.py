from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter

from apps.v1.school.models import School
from apps.v1.school.serializers.school import SchoolSerializer


class SchoolViewSet(viewsets.ModelViewSet):

    """ Viewset for crud of school  model """

    queryset = School.objects.all()
    serializer_class = SchoolSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'established_date', )
    ordering_fields = ('name', 'established_date', )
