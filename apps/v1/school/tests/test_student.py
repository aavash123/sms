import json

from django.urls import reverse
from django.utils.datetime_safe import datetime
from rest_framework import status

from apps.v1.school.serializers.student import StudentSerializer
from utils.constants import GRADE_CHOCIES, SECTION_CHOICES
from utils.test import SMSTestCase
from ..models import Student, School


class StudentTest(SMSTestCase):

    """ Test module for Student model """

    def setUp(self):
        super().setUp()
        self.create_students()

        self.valid_data = {
            "first_name": "Aavash",
            "last_name": "Khatri",
            "grade": GRADE_CHOCIES[0][1],
            "section": SECTION_CHOICES[1][1],
        }

        self.invalid_data = {
            "first_name": "Aavash",
            "last_name": "Khatri",
            "grade": "Bad Grade",
            "section": "Bad Section",
        }

    def test_get_all_student(self):
        url = reverse('school:student-api-list', kwargs={'school_id': School.objects.first().id})
        response = self.client.get(url)
        students_db = Student.objects.all()
        serializer = StudentSerializer(students_db, many=True)
        for index, item in enumerate(serializer.data):
            for key in item.keys():
                if key == 'school':
                    self.assertEqual(response.data[index][key]['id'], serializer.data[index][key])
                else:
                    self.assertEqual(response.data[index][key], serializer.data[index][key])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_student(self):
        url = reverse('school:student-api-detail', kwargs={'school_id': School.objects.first().id,
                                                           'pk': Student.objects.first().id})
        response = self.client.get(url)
        student_db = Student.objects.get(pk=1)
        serializer = StudentSerializer(student_db)
        for key in serializer.data.keys():
            if key == 'school':
                self.assertEqual(response.data[key]['id'], serializer.data[key])
            else:
                self.assertEqual(response.data[key], serializer.data[key])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_valid_student(self):
        url = reverse('school:student-api-list', kwargs={'school_id': School.objects.first().id})
        valid_response = self.client.post(url,  data=json.dumps(self.valid_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_201_CREATED)
        invalid_response = self.client.post(url,  data=json.dumps(self.invalid_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_student(self):
        valid_update_data = {
            "first_name": "Naya Aavash",
            "last_name": "Khatri",
            "grade": GRADE_CHOCIES[3][1],
            "section": SECTION_CHOICES[4][1],
        }
        invalid_update_data = {
            "first_name": "Aavash",
            "last_name": "Khatri",
            "grade": "Bad Choices",
            "section": SECTION_CHOICES[1][1],
        }
        url = reverse('school:student-api-detail', kwargs={'school_id': School.objects.first().id,
                                                           'pk': Student.objects.first().id})
        valid_response = self.client.put(url, data=json.dumps(valid_update_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_200_OK)
        invalid_response = self.client.put(url, data=json.dumps(invalid_update_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_student(self):
        url = reverse('school:student-api-detail', kwargs={'school_id': School.objects.first().id,
                                                           'pk': Student.objects.first().id})
        valid_response = self.client.delete(url)
        self.assertEqual(valid_response.status_code, status.HTTP_204_NO_CONTENT)
        invalid_response = self.client.delete(reverse('school:student-api-detail',
                                                      kwargs={'school_id': School.objects.first().id,
                                                              'pk': 30}))
        self.assertEqual(invalid_response.status_code, status.HTTP_404_NOT_FOUND)



    @staticmethod
    def create_students():
        School.objects.create(name="School ", established_date=datetime.now())
        entries = list()
        for i in range(0, 5):
            entries.append(Student(first_name="Aavash" + str(i),
                                   last_name="Khatri" + str(i),
                                   school=School.objects.first(),
                                   grade=GRADE_CHOCIES[0][1],
                                   section=SECTION_CHOICES[1][1]
                                   ))
        Student.objects.bulk_create(entries)
