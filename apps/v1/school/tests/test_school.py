import json

from django.urls import reverse
from django.utils.datetime_safe import datetime
from rest_framework import status

from apps.v1.school.serializers.school import SchoolSerializer
from utils.test import SMSTestCase
from ..models import School


class SchoolTest(SMSTestCase):
    """ Test module for School model """

    def setUp(self):
        super().setUp()
        self.create_schools()

        self.valid_data = {
            "name": "School Name",
            "description": "This is the description",
            "established_date": '2018-11-11'
        }

        self.invalid_data = {
            'name': 'School Name',
            'established_date': 'Bad date'
        }

    def test_get_all_school(self):
        url = reverse('school:school-api-list')
        response = self.client.get(url)
        schools_db = School.objects.all()
        serializer = SchoolSerializer(schools_db, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_school(self):
        url = reverse('school:school-api-detail', kwargs={'pk': 1})
        response = self.client.get(url)
        school_db = School.objects.get(pk=1)
        serializer = SchoolSerializer(school_db)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_valid_school(self):
        url = reverse('school:school-api-list')
        valid_response = self.client.post(url,  data=json.dumps(self.valid_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_201_CREATED)
        invalid_response = self.client.post(url,  data=json.dumps(self.invalid_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_school(self):
        valid_update_data = {
            "name": "New School Name",
            "description": "This is the updated description",
            "established_date": '2051-11-22'
        }
        invalid_update_data = {
            "name": "New School Name",
            "description": 22,
            "established_date": 'Bad date'
        }
        url = reverse('school:school-api-detail', kwargs={'pk': 1})
        valid_response = self.client.put(url, data=json.dumps(valid_update_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_200_OK)
        invalid_response = self.client.put(url, data=json.dumps(invalid_update_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_school(self):
        url = reverse('school:school-api-detail', kwargs={'pk': 4})
        valid_response = self.client.delete(url)
        self.assertEqual(valid_response.status_code, status.HTTP_204_NO_CONTENT)
        invalid_response = self.client.delete(reverse('school:school-api-detail', kwargs={'pk': 30}))
        self.assertEqual(invalid_response.status_code, status.HTTP_404_NOT_FOUND)

    @staticmethod
    def create_schools():
        entries = list()
        for i in range(0, 5):
            entries.append(School(name="School " + str(i), established_date = datetime.now()))
        School.objects.bulk_create(entries)
