import json

from django.urls import reverse
from django.utils.datetime_safe import datetime
from rest_framework import status

from apps.v1.school.serializers.fee import FeeSerializer
from utils.constants import GRADE_CHOCIES, SECTION_CHOICES
from utils.test import SMSTestCase
from utils.year_choice_gen import year_choices
from ..models import Fee, School, Student

YEAR_CHOICES = year_choices()


class FeeTest(SMSTestCase):

    """ Test module for Fee model """

    def setUp(self):
        super().setUp()
        self.create_fees(5)
        self.valid_data = {
            "due_amount": 8000,
            "paid_amount": 9000,
            "month": 1,
            "year": YEAR_CHOICES[0][0]
        }

        self.invalid_data = {
            "due_amount": 8000,
            "paid_amount": 9000,
            "month": "January",
            "year": YEAR_CHOICES[0][0]
        }

    def test_get_all_fee(self):
        url = reverse('school:fee-api-list', kwargs={'student_id': Student.objects.first().id})
        response = self.client.get(url)
        fees_db = Fee.objects.all()
        serializer = FeeSerializer(fees_db, many=True)
        for index, item in enumerate(serializer.data):
            for key in item.keys():
                if key == 'student':
                    self.assertEqual(response.data[index][key]['id'], serializer.data[index][key])
                else:
                    self.assertEqual(response.data[index][key], serializer.data[index][key])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_fee(self):
        url = reverse('school:fee-api-detail', kwargs={'student_id': Student.objects.first().id,
                                                       'pk': Fee.objects.first().id})
        response = self.client.get(url)
        fee_db = Fee.objects.get(pk=1)
        serializer = FeeSerializer(fee_db)
        for key in serializer.data.keys():
            if key == 'student':
                self.assertEqual(response.data[key]['id'], serializer.data[key])
            else:
                self.assertEqual(response.data[key], serializer.data[key])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_valid_fee(self):
        url = reverse('school:fee-api-list', kwargs={'student_id': Student.objects.first().id})
        valid_response = self.client.post(url,  data=json.dumps(self.valid_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_201_CREATED)
        invalid_response = self.client.post(url,  data=json.dumps(self.invalid_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_fee(self):
        valid_update_data = {
            "due_amount": 99999,
            "paid_amount": 9000,
            "month": 5,
            "year": YEAR_CHOICES[0][0]
        }
        invalid_update_data = {
            "due_amount": 8000,
            "paid_amount": 9000,
            "month": "Jan",
            "year": None
        }
        url = reverse('school:fee-api-detail', kwargs={'student_id': Student.objects.first().id,
                                                           'pk': Fee.objects.first().id})
        valid_response = self.client.put(url, data=json.dumps(valid_update_data), content_type='application/json')
        self.assertEqual(valid_response.status_code, status.HTTP_200_OK)
        invalid_response = self.client.put(url, data=json.dumps(invalid_update_data), content_type='application/json')
        self.assertEqual(invalid_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_fee(self):
        url = reverse('school:fee-api-detail', kwargs={'student_id': Student.objects.first().id,
                                                           'pk': Fee.objects.first().id})
        valid_response = self.client.delete(url)
        self.assertEqual(valid_response.status_code, status.HTTP_204_NO_CONTENT)
        invalid_response = self.client.delete(reverse('school:fee-api-detail',
                                                      kwargs={'student_id': Student.objects.first().id,
                                                              'pk': 30}))
        self.assertEqual(invalid_response.status_code, status.HTTP_404_NOT_FOUND)



    @staticmethod
    def create_fees(x):
        School.objects.create(name="Magic school", established_date=datetime.now())
        Student.objects.create(first_name="John",
                               last_name="Cena",
                               school=School.objects.first(),
                               grade=GRADE_CHOCIES[0][1],
                               section=SECTION_CHOICES[1][1]
                               )
        entries = list()
        for i in range(0, 5):
            entries.append(Fee(student=Student.objects.first(),
                               due_amount=8000,
                               paid_amount=1000,
                               month=1,
                               year=YEAR_CHOICES[0][0]))
        Fee.objects.bulk_create(entries)
