from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.v1.school.views.fee import FeeViewSet
from apps.v1.school.views.school import SchoolViewSet
from apps.v1.school.views.student import StudentViewSet

router = DefaultRouter()

router.register('', SchoolViewSet, base_name='school-api')
router.register('(?P<school_id>\d+)/student', StudentViewSet, base_name='student-api')
router.register('student/(?P<student_id>\d+)/fee', FeeViewSet, base_name='fee-api')

urlpatterns = [
    path('', include(router.urls))
]
