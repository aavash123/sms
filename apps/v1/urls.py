from django.urls import path, include


urlpatterns = [
    path('accounts/', include(('apps.v1.accounts.urls', 'accounts'), namespace='accounts')),
    path('school/', include(('apps.v1.school.urls', 'school'), namespace='school')),
]
