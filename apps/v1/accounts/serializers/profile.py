from django.utils import timezone

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.v1.accounts.models import User, Profile


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=150, validators=[
        UniqueValidator(
            queryset=User.objects.all(),
            lookup='iexact',
            message='User with this email already exists'
        )
    ])

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'date_of_birth', 'gender')

    def validate(self, data):
        date_of_birth = data.get('date_of_birth')
        if not date_of_birth:
            raise serializers.ValidationError("Date of birth is required ")

        if date_of_birth > timezone.now().date() - timezone.timedelta():
            raise serializers.ValidationError("User cannot be smaller than 18 years ")

        return data


class ProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(default=False,)
    user = UserSerializer()

    class Meta:
        model = Profile
        fields = ('user', 'image', 'password', 'id')
        extra_kwargs = {'password': {'write_only': True},
                        'id': {'read_only': True}}

    def validate_password(self, password):
        if not password and not self.instance:
            raise serializers.ValidationError("Password is required")
        return password

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_data.update({
            'email': user_data.pop('email'),
            'first_name': user_data.pop('first_name'),
            'last_name': user_data.pop('last_name'),
            'date_of_birth': user_data.pop('date_of_birth') if user_data.get('date_of_birth') else None,
            'gender': user_data.pop('gender')
        })
        password = validated_data.get('password')
        user = User(**user_data)
        user.set_password(password)
        user.save()
        validated_data['user_id'] = user.id
        validated_data['password'] = password
        return super().create(validated_data)

    def update(self, instance, validated_data):
        user_data = validated_data.pop('user')
        user_data.update({
            'email': user_data.pop('email'),
            'first_name': user_data.pop('first_name'),
            'last_name': user_data.pop('last_name'),
            'date_of_birth': user_data.pop('date_of_birth') if user_data.get('date_of_birth') else None,
            'gender': user_data.pop('gender')
        })
        validated_data['password'] = instance.password
        User.objects.filter(id=instance.user_id).update(**user_data)
        return super().update(instance, validated_data)
