from django.urls import path, include

from apps.v1.accounts.views.profile import ProfileViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('profile', ProfileViewSet, base_name='profile')

urlpatterns = [
    path('', include(router.urls))
]