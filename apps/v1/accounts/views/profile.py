from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter

from apps.v1.accounts.models import Profile
from apps.v1.accounts.serializers.profile import ProfileSerializer


class ProfileViewSet(viewsets.ModelViewSet):

    """Handles creating and updating users """

    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('email', 'first_name', )
    ordering_fields = ('first_name', 'email', )
