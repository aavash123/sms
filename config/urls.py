from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from rest_framework_jwt.views import obtain_jwt_token


schema_view = get_swagger_view(title='SMS API')

urlpatterns = [
    path('admin/', admin.site.urls),

    # api apps urls
    path('api/v1/', include('apps.v1.urls')),

    # browsable api
    path('api/auth/', include('rest_framework.urls')),

    # swagger view
    path('v1/root', schema_view),

    # jwt token
    path('login/token/', obtain_jwt_token),
]
